#include "Car.h"

ACar::ACar() {
	PrimaryActorTick.bCanEverTick = true;

	body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
}

void ACar::init(float min, float max) {
	isActive = true;
	preferredSpeed = FMath::RandRange(min, max);
	nextCar = nullptr;

	currentSpeed = 0.0f;
}

void ACar::setActive(bool value) {
	isActive = value;

	SetActorHiddenInGame(!value);
	SetActorEnableCollision(value);
	SetActorTickEnabled(value);

	if(!value) {
		nextCar = nullptr;
	}
}

void ACar::BeginPlay() {
	Super::BeginPlay();
}

void ACar::Tick(float deltaTime) {
	Super::Tick(deltaTime);

	auto location = GetActorLocation();
	auto direction = GetActorForwardVector();

	location += direction * currentSpeed * deltaTime;

	SetActorLocation(location);

	if(nextCar) {
		if(nextCar->isActive) {
			adjustSpeed(nextCar->GetActorLocation() - location, deltaTime);
		} else {
			nextCar = nullptr;
		}
	} else {
		accelerate(deltaTime);
		findNextCar(location, direction);
	}
}

void ACar::accelerate(float deltaTime) {
	if(currentSpeed < preferredSpeed) {
		currentSpeed += acceleration * deltaTime;

		if(currentSpeed > preferredSpeed) {
			currentSpeed = preferredSpeed;
		}
	}
}

void ACar::adjustSpeed(FVector deltaVector, float deltaTime) {
	if(deltaVector.SizeSquared() < safeDistanceSquared) {
		currentSpeed -= deceleration * deltaTime;
	} else if(currentSpeed < nextCar->currentSpeed) {
		currentSpeed += acceleration * deltaTime;

		if(currentSpeed > nextCar->currentSpeed) {
			currentSpeed = nextCar->currentSpeed;
		}
	}

	if(currentSpeed < 0.0f) {
		currentSpeed = 0.0f;
	}
}

void ACar::findNextCar(FVector location, FVector direction) {
	auto target = direction * safeDistance + location;

	FHitResult hit;
	FCollisionQueryParams collisionQueryParams;

	collisionQueryParams.AddIgnoredActor(this);

	if(GetWorld()->LineTraceSingleByChannel(hit, location, target, ECC_Visibility, collisionQueryParams)) {
		if(hit.bBlockingHit) {
			nextCar = Cast<ACar>(hit.GetActor());

			safeDistanceSquared = (nextCar->GetActorLocation() - location).SizeSquared();
		}
	}
}