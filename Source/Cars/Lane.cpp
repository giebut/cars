#include "Lane.h"

ALane::ALane() {
	PrimaryActorTick.bCanEverTick = true;

	root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = root;

	start = CreateDefaultSubobject<UBoxComponent>(TEXT("Start"));
	start->SetupAttachment(RootComponent);

	end = CreateDefaultSubobject<UBoxComponent>(TEXT("End"));
	end->SetupAttachment(RootComponent);

	splineMesh = CreateDefaultSubobject<USplineMeshComponent>(TEXT("SplineMesh"));
	splineMesh->SetupAttachment(RootComponent);
}

void ALane::BeginPlay() {
	Super::BeginPlay();

	time = 0.0f;
	number = 0;

	startLocation = start->GetComponentLocation();
	endLocation = end->GetComponentLocation();

	direction = endLocation - startLocation;
	direction.Normalize();

	splineMesh->SetStartAndEnd(
		start->GetComponentLocation(),
		direction,
		end->GetComponentLocation(),
		direction
	);
}

void ALane::Tick(float deltaTime) {
	Super::Tick(deltaTime);

	if(number < limit) {
		spawnCar();
	}

	hideCar();

	time -= deltaTime;
}

void ALane::spawnCar() {
	TArray<AActor*> actors;
	start->GetOverlappingActors(actors, ACar::StaticClass());

	if(actors.Num() == 0 && time < 0.0f && minInterval >= 0.0f && maxInterval >= minInterval) {
		time = FMath::RandRange(minInterval, maxInterval);

		if(carTemplate) {
			FActorSpawnParameters actorSpawnParameters;
			ACar*car;

			auto location = startLocation + offset * FVector::UpVector;

			if(carPool.Num() > 0) {
				car = carPool[0];

				car->setActive(true);
				car->SetActorLocation(location);
				car->SetActorRotation(direction.Rotation());

				carPool.RemoveAt(0);
			} else {
				car = GetWorld()->SpawnActor<ACar>(carTemplate, location, direction.Rotation(), actorSpawnParameters);
			}

			if(car) {
				car->init(minSpeed, maxSpeed);
				number++;
			}
		}
	}
}

void ALane::hideCar() {
	TArray<AActor*> actors;
	end->GetOverlappingActors(actors, ACar::StaticClass());

	if(actors.Num() > 0) {
		auto car = Cast<ACar>(actors[0]);

		if(car) {
			car->setActive(false);

			carPool.Add(car);
		}

		number--;
	}
}