#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Car.generated.h"

UCLASS()
class CARS_API ACar : public AActor {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float safeDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float deceleration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float acceleration;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* body;

	//Collider

	ACar();

	void init(float, float);
	void setActive(bool);

	virtual void Tick(float) override;

protected:
	virtual void BeginPlay() override;

private:
	bool isActive;
	float currentSpeed;
	float preferredSpeed;
	float safeDistanceSquared;
	ACar*nextCar;

	void accelerate(float);
	void adjustSpeed(FVector, float);
	void findNextCar(FVector, FVector);
};