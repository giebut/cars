#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include <memory>

#include "Car.h"
#include "Components/BoxComponent.h"
#include "Components/SplineMeshComponent.h"

#include "Lane.generated.h"

UCLASS()
class CARS_API ALane : public AActor {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int limit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float maxInterval;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float minInterval;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float maxSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float minSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<ACar> carTemplate;

	USceneComponent*root;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent*start;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent*end;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		USplineMeshComponent*splineMesh;

	ALane();

	virtual void Tick(float) override;
protected:
	virtual void BeginPlay() override;
private:
	const float offset = 50;

	int number;
	float time;
	FVector startLocation;
	FVector endLocation;
	FVector direction;
	TArray<ACar*> carPool;

	void spawnCar();
	void hideCar();
};